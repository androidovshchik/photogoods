package io.androidovshchik.photogoods

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.os.Bundle
import android.support.media.ExifInterface
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.budiyev.android.codescanner.*
import com.snatik.storage.Storage
import com.svenkapudija.imageresizer.ImageResizer
import com.svenkapudija.imageresizer.operations.ImageRotation
import com.svenkapudija.imageresizer.utils.ImageWriter
import timber.log.Timber
import java.io.ByteArrayOutputStream

class ScannerActivity : AppCompatActivity() {

    private lateinit var codeScanner: CodeScanner

    private lateinit var storage: Storage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)
        storage = Storage(applicationContext)
        val scannerView = findViewById<CodeScannerView>(R.id.scanner_view)
        codeScanner = CodeScanner(this, scannerView)
        codeScanner.isAutoFocusEnabled = true
        codeScanner.isFlashEnabled = false
        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = CodeScanner.ALL_FORMATS
        codeScanner.scanMode = ScanMode.SINGLE
        codeScanner.setAutoFocusInterval(1000)
        codeScanner.decodeCallback = DecodeCallback {
            var index = 1
            val files = storage.getFiles(storage.getTempPath())
            files?.forEach { file ->
                Timber.d(file.path)
                val filename = if (index == 1) {
                    it.toString()
                } else {
                    "$it($index)"
                }
                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeFile(file.path, options)
                val exif = ExifInterface(file.path)
                when (exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> {
                        processFile(file.path, filename, 90, ImageRotation.CW_90, options.outHeight,
                            options.outWidth)
                    }
                    ExifInterface.ORIENTATION_ROTATE_180 -> {
                        processFile(file.path, filename, 180, ImageRotation.CW_180, options.outWidth,
                            options.outHeight)
                    }
                    ExifInterface.ORIENTATION_ROTATE_270 -> {
                        processFile(file.path, filename, 270, ImageRotation.CW_270, options.outHeight,
                            options.outWidth)
                    }
                    else -> {
                        processFile(file.path, filename, 0, null, options.outWidth,
                            options.outHeight)
                    }
                }
                index++
            }
            runOnUiThread {
                AlertDialog.Builder(this)
                    .setTitle("Фотообработка закончена")
                    .setMessage("Отсканированный код: $it\nКоличество фотографий: ${files.size}")
                    .setNegativeButton(getString(android.R.string.cancel)) { _, _ ->
                        codeScanner.startPreview()
                    }
                    .setPositiveButton(getString(android.R.string.ok)) { _, _ ->
                        setResult(RESULT_OK)
                        finish()
                    }
                    .create()
                    .show()
            }
        }
        codeScanner.errorCallback = ErrorCallback {
            runOnUiThread {
                Toast.makeText(this, "Camera initialization error: ${it.message}",
                    Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun processFile(filePath: String, filename: String, degrees: Int, rotation: ImageRotation?,
                            width: Int, height: Int) {
        Timber.d("> rotation $degrees")
        val newSize = if (height > width) {
            Math.round(600f * height / width)
        } else {
            Math.round(600f * width / height)
        }
        Timber.d("> newSize $newSize")
        Timber.d("> correct resolution [${width}x$height] WxH")
        val resized = if (degrees % 180 == 0) {
            Timber.d("> incorrect resolution none")
            ImageResizer.resize(storage.getFile(filePath), 600, newSize)
        } else {
            Timber.d("> incorrect resolution [${height}x$width] WxH")
            ImageResizer.resize(storage.getFile(filePath), newSize, 600)
        }
        Timber.d("> resized [${resized.width}x${resized.height}] WxH")
        val rotated = if (rotation == null) {
            resized.copy(resized.config, true)
        } else {
            ImageResizer.rotate(bitmap2Bytes(resized), rotation)
        }
        Timber.d("> rotated [${rotated.width}x${rotated.height}] WxH")
        val cropped = if (height > width) {
            Bitmap.createBitmap(rotated, 0, (newSize - 600) / 2, 600, 600)
        } else {
            Bitmap.createBitmap(rotated, (newSize - 600) / 2, 0, 600, 600)
        }
        Timber.d("> cropped [${cropped.width}x${cropped.height}] WxH")
        ImageWriter.writeToFile(cropped, storage.getWorkFile(filename))
        notifyGallery(storage.getWorkFilePath(filename))
        resized.recycle()
        rotated.recycle()
        cropped.recycle()
    }

    private fun bitmap2Bytes(bitmap: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        return stream.toByteArray()
    }

    @Suppress("unused")
    private fun overlayBitmapToCenter(bitmap: Bitmap, overlay: Bitmap): Bitmap {
        val marginLeft = bitmap.width * 0.5f - overlay.width * 0.5f
        val marginTop = bitmap.height * 0.5f - overlay.height * 0.5f
        val overlayBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height,
            Bitmap.Config.ARGB_8888)
        val canvas = Canvas(overlayBitmap)
        canvas.drawBitmap(bitmap, Matrix(), null)
        canvas.drawBitmap(overlay, marginLeft, marginTop, null)
        return overlayBitmap
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }
}
