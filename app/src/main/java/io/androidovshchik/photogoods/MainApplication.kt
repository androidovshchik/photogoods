@file:Suppress("unused")

package io.androidovshchik.photogoods

import com.github.androidovshchik.BaseApplication
import com.github.androidovshchik.Environment

class MainApplication: BaseApplication() {

    override val environment = Environment.DEVELOP

    override val theme = R.style.LibraryTheme_SupportDialog
}