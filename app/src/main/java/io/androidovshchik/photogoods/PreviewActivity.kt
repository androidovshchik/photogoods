package io.androidovshchik.photogoods

import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.signature.ObjectKey
import com.snatik.storage.Storage
import kotlinx.android.synthetic.main.activity_preview.*

class PreviewActivity : AppCompatActivity() {

    private lateinit var storage: Storage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)
        storage = Storage(applicationContext)
        val index = intent.getIntExtra("index", 0)
        notifyGallery(storage.getTempFilePath(index))
        close.setOnClickListener {
            setResult(RESULT_CANCELED)
            finish()
        }
        done.setOnClickListener {
            setResult(RESULT_OK)
            finish()
        }
        GlideApp.with(applicationContext)
            .load(Uri.parse("file://${storage.getTempFilePath(index)}"))
            .signature(ObjectKey(System.currentTimeMillis()))
            .into(image)
    }
}
