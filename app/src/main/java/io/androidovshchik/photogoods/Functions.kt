@file:Suppress("unused")

package io.androidovshchik.photogoods

import android.content.Context
import android.content.Intent
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import com.snatik.storage.Storage
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


val formatter = SimpleDateFormat("yyyy.MM.dd", Locale.getDefault())

fun Storage.getAppFolder(): String {
    val directory = this.externalStorageDirectory + File.separator + "PhotoGoods"
    if (!isDirectoryExists(directory)) {
        createDirectory(directory)
    }
    return directory
}

fun Storage.getWorkFile(filename: String): File {
    return getFile(getWorkFilePath(filename))
}

fun Storage.getWorkFilePath(filename: String): String {
    return getWorkPath() + File.separator + "$filename.jpg"
}

fun Storage.getWorkPath(): String {
    val directory = getAppFolder() + File.separator + formatter.format(Date())
    if (!isDirectoryExists(directory)) {
        createDirectory(directory)
    }
    return directory
}

fun Storage.getTempFile(index: Int): File {
    return getFile(getTempFilePath(index))
}

fun Storage.getTempFilePath(index: Int): String {
    return getTempPath() + File.separator + "$index.jpg"
}

fun Storage.getTempPath(): String {
    val directory = getAppFolder() + File.separator + "temp"
    if (!isDirectoryExists(directory)) {
        createDirectory(directory)
    }
    return directory
}

fun Context.notifyGallery(path: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        MediaScannerConnection.scanFile(this, arrayOf(path), null, null)
    } else {
        sendBroadcast(Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://$path")))
    }
}