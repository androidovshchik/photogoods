package io.androidovshchik.photogoods

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.github.androidovshchik.BaseV7Activity
import com.github.androidovshchik.utils.PermissionUtil
import com.snatik.storage.Storage
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.CameraConfiguration
import io.fotoapparat.log.logcat
import io.fotoapparat.selector.back
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class MainActivity : BaseV7Activity() {

    private lateinit var storage: Storage

    private lateinit var fotoapparat: Fotoapparat

    private var index = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = "Фотографирование №$index"
        storage = Storage(applicationContext)
        fotoapparat = Fotoapparat(
            context = this,
            view = cameraView,
            focusView = focusView,
            logger = logcat(),
            lensPosition = back(),
            cameraConfiguration = CameraConfiguration.standard(),
            cameraErrorCallback = {
                Timber.e(it)
            }
        )
        fab.setOnClickListener {
            fab.isEnabled = false
            if (PermissionUtil.areGranted(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA)) {
                takePicture(index)
            }
        }
        finish.setOnClickListener {
            val intent = Intent(applicationContext, ScannerActivity::class.java)
            startActivityForResult(intent, 2)
        }
    }

    private fun takePicture(index: Int) {
        fotoapparat.autoFocus()
            .takePicture()
            .saveToFile(storage.getTempFile(index))
            .whenAvailable {
                fab.isEnabled = true
                val intent = Intent(applicationContext, PreviewActivity::class.java)
                intent.putExtra("index", index)
                startActivityForResult(intent, 1)
            }
    }

    override fun onStart() {
        super.onStart()
        finish.visibility = if (index > 1) View.VISIBLE else View.GONE
        PermissionUtil.request(this, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .subscribe {
                if (it) {
                    fotoapparat.start()
                }
            }
    }

    override fun onStop() {
        super.onStop()
        if (PermissionUtil.isGranted(applicationContext, Manifest.permission.CAMERA)) {
            fotoapparat.stop()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                index++
            } else {
                val path = storage.getTempFilePath(index)
                if (storage.isFileExist(path)) {
                    storage.deleteFile(path)
                }
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                index = 1
                storage.deleteDirectory(storage.getTempPath())
            }
        }
        title = "Фотографирование №$index"
    }
}
